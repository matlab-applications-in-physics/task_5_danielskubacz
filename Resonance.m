%%MATLAB 2020b
%Name: Resonance
%Author: Daniel Skubacz
%Date: 14.12.2020

%Defining data (values and constants):
r = (1/12)*(0.0254); % (m) radius of wire
n = 40; % number of scrolls
m = 0.1; % (kg) mass
t = 10; % (s) time
G = 8e10; % (Pa)
R = [0.005:0.002:0.05]; % (m) radius of spring
g = 9.80665; % (m/s) graviotional acceleration from 'https://en.wikipedia.org/wiki/Gravitational_acceleration'
f = [0.01:0.03:0.30]; % (Hz) frequency
F = m*g; % (N) Maximum force of mass 'm'

%Counting beta, spring cosntant and omega:
beta = 1/(2*t);
k = (G*r^4)./(4*n*R.^3);
omega_s = sqrt(k./m);
omega_f = 2*pi.*f;

%Counting amplitude:
for i=1:size(R);
   A(i) = F./(m.*sqrt((omega_s.^2 - omega_f.^2).^2+4*beta.^2*omega_f.^2));
end;
